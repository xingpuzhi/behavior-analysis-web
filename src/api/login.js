import axios from "@/plugins/axios";

export function login(data) {
    return axios.post(`/login?username=${data.username}&password=${data.password}`)
}

export function getCurrentUser() {
    return axios.get('/user/currentUser')
}

export function checkToken() {
    return axios.get('/check/token')
}
