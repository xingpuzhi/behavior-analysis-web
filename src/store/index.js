import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        payload: '',
    },
    mutations: {
        SET_PAYLOAD(state, payload) {
            state.payload = payload;
        },
    },
    actions: {

    },
    modules: {}
})
