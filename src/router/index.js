import Vue from 'vue'
import VueRouter from 'vue-router'
import {message} from 'ant-design-vue';

Vue.use(VueRouter)

//无需鉴权的页面
const NO_AUTHENTICATION_ROUTERS = ['/', '/demo/water', '/demo/relation', '/demo/ws'];

const routes = [
    {
        path: '/',
        name: 'Login',
        component: () => import('@/views/login/Login.vue'),
    },

    {
        path: '/home',
        name: 'Home',
        component: () => import('@/views/Home.vue'),
        children: [
            {
                path: '/index',
                name: 'Index',
                meta: {
                    role: 1
                },
                component: () => import('@/views/index/Index.vue'),
            },
            {
                path: '/test',
                name: 'Test',
                meta: {
                    role: 2
                },
                component: () => import('@/views/test/Test.vue'),
            },
        ]
    },


    {
        path: '/demo/water',
        name: 'Demo',
        component: () => import('@/views/demo/WaterfallDemo.vue'),
    },
    {
        path: '/demo/relation',
        name: 'Demo',
        component: () => import('@/views/demo/RelationGraphDemo.vue'),
    },
    {
        path: '/demo/ws',
        name: 'Websocket',
        component: () => import('@/views/demo/WebsocketDemo.vue'),
    },
]

const router = new VueRouter({
    // mode: 'history',
    base: process.env.BASE_URL,
    routes
})


router.beforeEach((to, from, next) => {
        if (NO_AUTHENTICATION_ROUTERS.indexOf(to.path) !== -1) {
            console.log('无需鉴权')
            next();
            return
        }
        let role = localStorage.getItem('role')
        console.log(role)
        const isAuthenticated = checkIfUserIsAuthenticated(); //判断用户是否登录

        if (!isAuthenticated) {
            //没有token，返回登录页面
            next({path: '/'});
        } else {
            //判断权限
            if (to.meta.role === 0) {
                next();
                return;
            }
            if (to.meta.role !== parseInt(role)) {
                message.warning('没有权限访问此页面')
            } else {
                next();
            }
        }
    }
)


function checkIfUserIsAuthenticated() {
    const token = localStorage.getItem('token'); // 从本地存储中获取 token
    return token !== null && token !== undefined;
}


export default router
