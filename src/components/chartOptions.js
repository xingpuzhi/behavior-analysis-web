export default {
    chart: {
        zoomType: 'x',
        backgroundColor: 'rgba(0,0,0,0)',
        polar: true,
        type: 'line'
    },
    resetZoomButton: {
        // theme: { style: { display: 'none'} }
    },
    reflow: true,
    credits: {
        //版权
        enabled: false
    },
    yAxis: {
        title: {
            enabled: false
        },
        gridLineColor: 'rgba(46, 54, 92, 0.69)',
        lineColor: 'rgba(46, 54, 92, 0.69)',
        labels: {
            style: {
                color: '#dfdfdf'
            }
        }
    },
    xAxis: {
        labels: {
            style: {
                color: '#dfdfdf'
            }
        },
        min: 950,
        max: 2150,
        showLastLabel: true,
        gridLineColor: 'rgba(46, 54, 92, 0.69)',
        lineColor: 'rgba(165,165,165, 0.3)',
        showFirstLabel: true,
        tickColor: false,
        plotBands: [
            {
                from: 1200, // 标示区开始值
                to: 1230, // 标示区结束值
                color: 'rgba(225, 90, 47, 0.3)', // 标示区背景颜色
                borderWidth: 0, // 标示区边框宽度
                borderColor: '#aca', // 标示区边框颜色
            },
            {
                from: 1450, // 标示区开始值
                to: 1500, // 标示区结束值
                color: 'rgba(225, 90, 47, 0.3)', // 标示区背景颜色
                borderWidth: 0, // 标示区边框宽度
                borderColor: '#aca', // 标示区边框颜色
            },
            {
                from: 1800, // 标示区开始值
                to: 1850, // 标示区结束值
                color: 'rgba(225, 90, 47, 0.3)', // 标示区背景颜色
                borderWidth: 0, // 标示区边框宽度
                borderColor: '#aca', // 标示区边框颜色
            }
        ], //标注区
        plotLines: [
            {
                color: '#EB3C0A',
                width: 1,
                value: 1215,
                zIndex: 5,
                dashStyle: 'LongDash'
                // label:{
                //     text:"DVB",
                //     align:'left',
                //     x:-34,
                //     y:6,
                //     style:{color:"#EB3C0A"}
                // }
            },
            {
                color: '#EB3C0A',
                width: 1,
                value: 1475,
                zIndex: 5,
                dashStyle: 'LongDash'
            },
            {
                color: '#EB3C0A',
                width: 1,
                value: 1825,
                zIndex: 5,
                dashStyle: 'LongDash'
            }
        ]
    },
    title: {
        enabled: false,
        text: ''
    },
    boost: {
        useGPUTranslations: true
    },
    legend: {
        enabled: false
    },
    tooltip: {
        shared: true,
        crosshairs: true,
        headerFormat: '111',
        pointFormat: '222'
    },
    series: [
        {
            color: '#00ffff',
            marker: {
                enabled: false
            },
            animation: false,
            enableMouseTracking: false,
            type: 'line',
            data: [],
            lineWidth: 0.5
        }
    ]
}
