import axios from 'axios'
import {message} from 'ant-design-vue';

let request = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    timeout: 60000,
    withCredentials: true
})
//POST传参序列化(添加请求拦截器)
request.interceptors.request.use((config) => {
    //在发送请求之前做某件事
    // if (config.url === '/system/account/login') {
    //     config.headers['IDZH-APP-ID'] = 'idianzhihuiPms';
    // } else {
    //     config.headers['IDZH-APP-ID'] = 'idianzhihuiIot';
    // }
    if (config.url !== '/login') {
        config.headers['Authorization'] = localStorage.getItem('token');
    }
    return config;
}, (error) => {
    // eslint-disable-next-line no-console
    console.log('错误的传参')
    return Promise.reject(error);
});

//返回状态判断(添加响应拦截器)
request.interceptors.response.use((res) => {
    let url = res.config.url
    let result = res.data;
    if (result.code === 0) {
        return Promise.resolve(result.data);
    } else {
        message.warning(result.message);
        return Promise.reject("errcode:" + result.code + ",errmsg:" + result.message)
    }
}, (error) => {
    let status = error.response.status;
    switch (status) {
        case 400:
            message.warning(error.response.data.message)
            break;
        case 401:
            message.warning('登录已失效，请重新登录')
            localStorage.removeItem('token')
            break;
        case 403:
            message.warning(error.response.data.message)
            break;
        case 500:
            message.error('系统异常');
            break;
    }
    return Promise.reject(error);
});

let get = (url, params) => request.get(url, {params});
let post = (url, data) => request.post(url, data);
let put = (url, data) => request.put(url, data);
let deletes = (url, data) => request.delete(url, {data});
let postMultipart = (url, data) => request.post(url, data, {
    headers: {
        credentials: 'same-origin',
    },
});

export default {
    get, post, put, deletes, postMultipart
}
