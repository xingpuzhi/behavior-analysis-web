const {defineConfig} = require('@vue/cli-service')
module.exports = defineConfig({
    publicPath: './',//打出来的包资源都会被链接为相对路径，可以被部署在任意路径
    transpileDependencies: true,
    lintOnSave: false,
    devServer: {
        client:{
            overlay: false
        },
        open: false,
        host: '0.0.0.0',
        proxy: {
            'api': {
                target: 'http://localhost:8900',
                // target: 'http://192.168.20.70:8300',
                changeOrigin: true,
                pathRewrite: {'^/api': ''},
            }
        }
    }
})
